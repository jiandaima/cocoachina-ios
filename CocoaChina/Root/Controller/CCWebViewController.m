//
//  CCWebViewController.m
//  CocoaChina
//
//  Created by Zhang on 15/10/29.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCWebViewController.h"
#import <WebKit/WebKit.h>
#import "CCNavigationBar.h"
#import "CCShareActionSheet.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "MJRefresh.h"
#import "JDStatusBarNotification.h"
#import "CCFavoriteService.h"
#import "NSDictionary+YJJsonString.h"

#define detailJS @"var styleTag = document.createElement('style');\
styleTag.textContent ='section.mthead{padding-top:0px;width:100%;margin-top:0px;}'+'header,footer,.bottom-ad{display:none;}';\
document.documentElement.appendChild(styleTag);"

@interface CCWebViewController ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler,CCShareActionSheetDelegate,GADBannerViewDelegate,GADInterstitialDelegate>
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) WKWebViewConfiguration *configuration;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIButton *navMoreButton;
@property (nonatomic, strong) UIButton *navBackButton;
@property (nonatomic, strong) UIButton *navCloseButton;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) GADBannerView *bannerView;
@property (nonatomic, strong) GADInterstitial *interstitial;
@property (nonatomic, strong) MASConstraint *webViewBottomConstraint;
@end

@implementation CCWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self commonInit];
    __weak CCWebViewController *weakSelf = self;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [[CCFavoriteService sharedCCFavoriteService]addFavorite:weakSelf.masterModel];
        [weakSelf handleFavorite];
    }];
    header.lastUpdatedTimeLabel.hidden = YES;
    [header setTitle:@"下拉收藏文章" forState:MJRefreshStateIdle];
    [header setTitle:@"收藏文章" forState:MJRefreshStatePulling];
    [header setTitle:@"收藏文章" forState:MJRefreshStateRefreshing];
    
    //self.webView.scrollView.mj_header = header;
    //进图条
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:@"CCWebViewController"];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.masterModel.newsLink]];
    [self.webView loadRequest:urlRequest];
    
    ///banner广告
    [self createBannerView];
    [self.bannerView loadRequest:[GADRequest request]];
    ///插屏广告
    //self.interstitial = [self createAndLoadInterstitial];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(viewDidBecomeActive:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)handleFavorite{
    
    if (![BmobUser getCurrentUser]) {
        [self.webView.scrollView.mj_header endRefreshing];
        [JDStatusBarNotification showWithStatus:@"只有先绑定了QQ号才能收藏呦~" dismissAfter:2 styleName:JDStatusBarStyleWarning];
        return;
    }
    __weak CCWebViewController *weakSelf = self;
    BmobObject *favoriteObject = [BmobObject objectWithClassName:@"favorite"];
    [favoriteObject setObject:self.masterModel.initalDictionary forKey:@"newsData"];
    [favoriteObject setObject:[BmobUser getCurrentUser].objectId forKey:@"uid"];
    [favoriteObject setObject:self.masterModel.newsId forKey:@"newsId"];
    [favoriteObject saveInBackgroundWithResultBlock:^(BOOL isSuccessful, NSError *error) {
        [weakSelf.webView.scrollView.mj_header endRefreshing];
        if (isSuccessful) {
            [JDStatusBarNotification showWithStatus:@"文章收藏成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
        }else{
            if (error.code == 401) {
                [JDStatusBarNotification showWithStatus:@"您已收藏了该文章" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
            }else if(error.code == BmobErrorTypeConnectFailed){
                [JDStatusBarNotification showWithStatus:@"网络连接失败了~" dismissAfter:2 styleName:JDStatusBarStyleError];
            }else{
                [JDStatusBarNotification showWithStatus:@"服务器好像出了点小问题~" dismissAfter:2 styleName:JDStatusBarStyleError];
            }
        }
    }];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressView.hidden = self.webView.estimatedProgress == 1;
        [self.progressView setProgress:self.webView.estimatedProgress animated:YES];
    }else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

-(void)commonInit{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.navBar];
    [self.view addSubview:self.webView];
    [self.view addSubview:self.progressView];
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom);
        make.height.equalTo(@2);
    }];
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom);
        self.webViewBottomConstraint = make.bottom.equalTo(self.view);
    }];
}

-(void)createBannerView{
    
    self.bannerView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
    self.bannerView.adUnitID = @"ca-app-pub-6979762357915263/3454332432";
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
}

- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-6979762357915263/4931065635"];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}

-(NSArray<id<UIPreviewActionItem>> *)previewActionItems{
    UIPreviewAction *action1 = [UIPreviewAction actionWithTitle:@"拷贝链接" style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        
        [[UIPasteboard generalPasteboard] setPersistent:YES];
        [[UIPasteboard generalPasteboard] setValue:self.masterModel.newsLink forPasteboardType:[UIPasteboardTypeListString objectAtIndex:0]];
        [JDStatusBarNotification showWithStatus:@"链接已拷贝" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
    }];
    
    UIPreviewAction *action2 =[UIPreviewAction actionWithTitle:@"用Safari打开" style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.masterModel.newsLink]]) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.masterModel.newsLink]];
        }
    }];
    return @[action1,action2];
}

#pragma mark - WKUIDelegate

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler{
    
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __nullable result))completionHandler{
    
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    if (navigationAction.navigationType == WKNavigationTypeLinkActivated) {
//        if([[UIApplication sharedApplication]canOpenURL:navigationAction.request.URL]){
//            [[UIApplication sharedApplication]openURL:navigationAction.request.URL];
//        }
    }else{
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    decisionHandler(WKNavigationActionPolicyCancel);
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
    
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    
}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView NS_AVAILABLE(10_11, 9_0){
    
}

#pragma mark - WKScriptMessageHandler

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    
    if ([message.name isEqualToString:@"didGetPosts"]) {
        NSArray *posts = message.body;
        NSLog(@"%@",posts);
    }
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView{
    if (!self.bannerView.superview) {
        [self.view addSubview:self.bannerView];
        [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
        }];
    }
    self.webViewBottomConstraint.offset(-bannerView.frame.size.height);
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    self.webView.scrollView.contentInset = UIEdgeInsetsZero;
    self.webView.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView{
    self.webView.scrollView.contentInset = UIEdgeInsetsZero;
    self.webView.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

#pragma mark - GADInterstitialDelegate

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

#pragma mark - action
-(void)viewDidBecomeActive:(NSNotification *)notification{
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    }
}

- (void)backButtonClicked:(UIButton *)sender {
    if (self.webView.canGoBack) {
        [self.webView goBack];
        if(!self.navBar.leftButtons){
            self.navBar.leftButtons = @[self.navBackButton,self.navCloseButton];
        }
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)closeButtonClicked:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)moreButtonClicked:(UIButton *)sender {
    
    [[CCShareActionSheet shareServiceWithController:self title:self.masterModel.newsTitle context:self.masterModel.newsTitle imageLink:self.masterModel.newsImage topicLink:self.masterModel.newsLink delegate:self]show];
}

#pragma mark - getters and setters

-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.leftButton = self.navBackButton;
        _navBar.rightButton = self.navMoreButton;
    }
    return _navBar;
}

-(UIButton *)navBackButton{
    if (!_navBackButton) {
        _navBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_navBackButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_navBackButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _navBackButton;
}

-(UIButton *)navCloseButton{
    if (!_navCloseButton) {
        _navCloseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_navCloseButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_navCloseButton setBackgroundImage:[UIImage imageNamed:@"top_navigation_close"] forState:UIControlStateNormal];
    }
    return _navCloseButton;
}

-(UIButton *)navMoreButton{
    if (!_navMoreButton) {
        _navMoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_navMoreButton addTarget:self action:@selector(moreButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_navMoreButton setBackgroundImage:[UIImage imageNamed:@"nav_more"] forState:UIControlStateNormal];
    }
    return _navMoreButton;
}

-(WKWebViewConfiguration *)configuration{
    if (!_configuration) {
        _configuration = [[WKWebViewConfiguration alloc]init];
        if (IOS_9_OR_LATER) {
            _configuration.applicationNameForUserAgent = @"CococaChina";
        }
        ///内容
        NSString *scriptUrl = [[NSBundle mainBundle] pathForResource:@"addScript" ofType:@"js"];
        NSString *scriptContent = [NSString stringWithContentsOfFile:scriptUrl encoding:NSUTF8StringEncoding error:nil];
        WKUserScript *script1 = [[WKUserScript alloc]initWithSource:scriptContent injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:YES];
        [_configuration.userContentController addUserScript:script1];
        
        WKUserScript *script = [[WKUserScript alloc]initWithSource:detailJS injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:YES];
        [_configuration.userContentController addUserScript:script];
    }
    return _configuration;
}

-(WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc]initWithFrame:CGRectZero configuration:self.configuration];
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
        if (IOS_9_OR_LATER) {
            _webView.customUserAgent = @"CocoaChina/1.0";
        }else{
            [_webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
                NSString *userAgent = result;
                NSString *newUserAgent = [userAgent stringByAppendingString:@" CocoaChina/1.0"];
                NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newUserAgent, @"UserAgent", nil];
                [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
                [_webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
                    NSLog(@"navigator.userAgent %@", result);
                }];
            }];

        }
    }
    return _webView;
}

-(UIProgressView *)progressView{
    if (!_progressView) {
        _progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressView.progressTintColor = [UIColor greenColor];
        _progressView.trackTintColor =[UIColor clearColor];
    }
    return _progressView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress" context:@"CCWebViewController"];
}

@end
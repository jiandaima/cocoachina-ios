//
//  YJAboutViewController.m
//  yuedu
//
//  Created by Zhang on 15/9/27.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCAboutViewController.h"
#import "UIScrollView+EmptyDataSet.h"
#import "CCNavigationBar.h"

@interface CCAboutViewController ()<DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *versionLabel;
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation CCAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self commonInit];
}

-(void)commonInit{
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.navBar];
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.bottom.left.right.equalTo(self.view);
    }];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    return [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Version %@",[YJTools appVersion]] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor yj_lightTextColor]}];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView{
    return [[NSMutableAttributedString alloc]initWithString:@"@by dami7786" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor yj_lightTextColor]}];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView{
    return 30;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    
    return -50;
}
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    
    return [UIImage imageNamed:@"icon_logo"];
}

- (UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state{
    
    if (state == UIControlStateNormal) {
        
        return [UIImage imageWithColor:[UIColor yj_cellBackgroundColor]];
    }else if (state == UIControlStateHighlighted){
        return [UIImage imageWithColor:[UIColor yj_cellSelectedColor]];
    }
    return nil;
}


- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView{
    
    return [UIColor whiteColor];
}

#pragma mark - DZNEmptyDataSetDelegate

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    
    return YES;
}


//- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView{
//
//}
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}
//- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView{
//
//}
- (void)emptyDataSetDidTapButton:(UIScrollView *)scrollView{
    
}
#pragma mark - UIScrollViewDelelage
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.navBar damNavigationBarDidScrollWithScrollView:scrollView];
}

#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

#pragma mark - Action
-(void)onBackButtonClicked:(UIButton *)backButton{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - getters and setters
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.emptyDataSetDelegate = self;
        _tableView.emptyDataSetSource= self;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.title = @"关于";
        _navBar.leftButton = self.backButton;
    }
    return _navBar;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(onBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
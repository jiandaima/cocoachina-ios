//
//  YJSetViewController.m
//  yuedu
//
//  Created by Zhang on 15/10/26.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCSetViewController.h"
#import "CCNavigationBar.h"
#import "iRate.h"
#import "CCShareViewController.h"
#import "SDImageCache.h"
#import "CCAboutViewController.h"
#import <UMengAnalytics/MobClick.h>
#import <UMengSocial/UMSocial.h>
#import "UMFeedback.h"
#import "CCSetCellModel.h"
#import "CCHistoryViewController.h"
#import "CCFavoriteViewController.h"
#import "CCRegistViewController.h"
#import "CCLoginViewController.h"

NSString *const kCellIdentifier = @"kCellIdentifier";

@interface CCSetViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) NSArray *dataSource;
@end

@implementation CCSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self commonInit];
}

-(void)commonInit{
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.navBar];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom);
    }];
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *sections = self.dataSource[section];
    return sections.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.textLabel.backgroundColor = [UIColor clearColor];
//    UILabel *subTitleLabel =  [cell.contentView viewWithTag:100];
//    if (indexPath.row == 0 && indexPath.section == 0) {
//        
//        //绑定QQ号
//        if (!subTitleLabel) {
//            subTitleLabel = [[UILabel alloc]init];
//            subTitleLabel.tag = 100;
//            subTitleLabel.font = [UIFont systemFontOfSize:16];
//            [cell.contentView addSubview:subTitleLabel];
//            [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.centerY.equalTo(cell.contentView);
//                make.right.equalTo(cell.contentView.mas_right);
//            }];
//        }
//        if([BmobUser getCurrentUser]){
//            subTitleLabel.textColor = [UIColor yj_redColor];
//            subTitleLabel.text = @"已绑定";
//        }else{
//            subTitleLabel.textColor = [UIColor yj_blueColor];
//            subTitleLabel.text = @"绑定";
//        }
//        
//    }else{
//        [subTitleLabel removeFromSuperview];
//    }
    
    
    CCSetCellModel *cellModel = self.dataSource[indexPath.section][indexPath.row];
    cell.textLabel.text = cellModel.title;
    cell.textLabel.textColor = [UIColor yj_darkTextColor];
    cell.imageView.image = cellModel.image;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CCSetCellModel *cellModel = self.dataSource[indexPath.section][indexPath.row];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:cellModel.selector];
#pragma clang diagnostic pop
}

#pragma mark - actions
-(void)onBackButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)snsLogin{
    
    CCLoginViewController *loginViewController = [[CCLoginViewController alloc]init];
    [self.navigationController pushViewController:loginViewController animated:YES];
    
    return;
    if([BmobUser getCurrentUser]){
        [BmobUser logout];
        [JDStatusBarNotification showWithStatus:@"解绑成功" dismissAfter:1.5 styleName:JDStatusBarStyleSuccess];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }else{
        __weak CCSetViewController *weakSelf = self;
        
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQQ];
                NSDictionary *authorDictionary =  @{@"access_token":snsAccount.accessToken,@"uid":snsAccount.usid,@"expirationDate":snsAccount.expirationDate,@"iconURL":snsAccount.iconURL};
                [BmobUser loginInBackgroundWithAuthorDictionary:authorDictionary platform:BmobSNSPlatformQQ block:^(BmobUser *user, NSError *error) {
                    if (error) {
                        [JDStatusBarNotification showWithStatus:error.userInfo[@"error"] dismissAfter:3 styleName:JDStatusBarStyleError];
                    }else{
                        [JDStatusBarNotification showWithStatus:@"绑定成功" dismissAfter:1.5 styleName:JDStatusBarStyleSuccess];
                        [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    }
                }];
            }});
    }
}

-(void)favoreteList{
    if (![BmobUser getCurrentUser]) {
        [JDStatusBarNotification showWithStatus:@"只有先绑定了QQ号才能查看我的收藏呦~" dismissAfter:2.5 styleName:JDStatusBarStyleError];
        return;
    }
    
    CCFavoriteViewController *favritateController = [[CCFavoriteViewController alloc]init];
    [self.navigationController pushViewController:favritateController animated:YES];
}

-(void)readHistory{
    CCHistoryViewController *historyController = [[CCHistoryViewController alloc]init];
    [self.navigationController pushViewController:historyController animated:YES];
}

-(void)clearCache{
    [[SDImageCache sharedImageCache] calculateSizeWithCompletionBlock:^(NSUInteger fileCount, NSUInteger totalSize) {
        NSByteCountFormatter *byteFormatter = [[NSByteCountFormatter alloc]init];
        byteFormatter.allowedUnits = NSByteCountFormatterUseKB|NSByteCountFormatterUseMB;
        byteFormatter.countStyle = NSByteCountFormatterCountStyleFile;
        NSString *sizeStr = [byteFormatter stringFromByteCount:totalSize];
        sizeStr = [sizeStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        [JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"共%@缓存，清除成功",sizeStr] dismissAfter:1.5 styleName:JDStatusBarStyleSuccess];
        [[SDImageCache sharedImageCache]clearDisk];
    }];
}

-(void)rate{
    if([[UIApplication sharedApplication]canOpenURL:[iRate sharedInstance].ratingsURL]){
        [[UIApplication sharedApplication]openURL:[iRate sharedInstance].ratingsURL];
    }
}

-(void)suggest{
    [self.navigationController pushViewController:[UMFeedback feedbackViewController] animated:YES];
}

-(void)shareToFriend{
    CCShareViewController *shareViewcontroller =  [[CCShareViewController alloc]init];
    [self.navigationController pushViewController:shareViewcontroller animated:YES];
}

-(void)about{
    CCAboutViewController *aboutViewController =  [[CCAboutViewController alloc]init];
    [self.navigationController pushViewController:aboutViewController animated:YES];
}

#pragma mark - getters and setters
-(NSArray *)dataSource{
    if (!_dataSource) {
        
        UIImage *snsImage = [UIImage imageNamed:@"set_sns"];
        CCSetCellModel *snsModel = [[CCSetCellModel alloc]init];
        snsModel.image = snsImage;
        snsModel.title = @"QQ账号";
        snsModel.selector = @selector(snsLogin);
        
        
        UIImage *favoriteImage = [UIImage imageNamed:@"set_favorite"];
        CCSetCellModel *favoriteModel = [[CCSetCellModel alloc]init];
        favoriteModel.title = @"我的收藏";
        favoriteModel.image = favoriteImage;
        favoriteModel.selector = @selector(favoreteList);
        
        UIImage *historyImage = [UIImage imageNamed:@"set_history"];
        CCSetCellModel *historyModel = [[CCSetCellModel alloc]init];
        historyModel.title = @"阅读历史";
        historyModel.image = historyImage;
        historyModel.selector = @selector(readHistory);
        
        ///清除缓存
        UIImage *cacheImage = [UIImage imageNamed:@"set_cache"];
        CCSetCellModel *trashModel = [[CCSetCellModel alloc]init];
        trashModel.title = @"清理缓存";
        trashModel.image = cacheImage;
        trashModel.selector = @selector(clearCache);
        
        ///好评
        UIImage *zanImage = [UIImage imageNamed:@"set_zan"];
        CCSetCellModel *rateModel = [[CCSetCellModel alloc]init];
        rateModel.title = @"给CocoaChina好评";
        rateModel.image = zanImage;
        rateModel.selector = @selector(rate);
        
        ///意见反馈
        UIImage *fanhuiImage = [UIImage imageNamed:@"set_fankui"];
        CCSetCellModel *suggestModel = [[CCSetCellModel alloc]init];
        suggestModel.title = @"意见反馈";
        suggestModel.image = fanhuiImage;
        suggestModel.selector = @selector(suggest);
        
        ///分享
        UIImage *shareImage = [UIImage imageNamed:@"set_share"];
        CCSetCellModel *shareModel = [[CCSetCellModel alloc]init];
        shareModel.title = @"分享给好友";
        shareModel.image = shareImage;
        shareModel.selector = @selector(shareToFriend);
        
        ///关于
        UIImage *aboutImage = [UIImage imageNamed:@"set_info"];
        CCSetCellModel *aboutModel = [[CCSetCellModel alloc]init];
        aboutModel.title = @"关于CocoaChina";
        aboutModel.image = aboutImage;
        aboutModel.selector = @selector(about);
        
        _dataSource = @[@[historyModel],@[trashModel],@[rateModel,suggestModel,shareModel],@[aboutModel]];
    }
    return _dataSource;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorColor = [UIColor yj_lineColor];
        _tableView.separatorInset = UIEdgeInsetsZero;
        _tableView.alwaysBounceVertical = YES;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 44;
        _tableView.backgroundColor = [UIColor yj_cellBackgroundColor];
        _tableView.sectionFooterHeight = 0;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
    }
    return _tableView;
}

-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.title = @"更多";
        _navBar.leftButton = self.backButton;
    }
    return _navBar;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(onBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    
}
@end
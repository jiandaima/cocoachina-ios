//
//  YJShareViewController.m
//  yuedu
//
//  Created by Zhang on 15/9/28.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCShareViewController.h"
#import "CCNavigationBar.h"
#import "CCShareItemCell.h"
#import "UMSocial.h"
#import "CCShareActionSheet.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>

@interface CCShareViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UMSocialUIDelegate>
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIImageView *codeImageView;
@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSArray *dataSource;
@end

@implementation CCShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self commonInit];
}

-(void)commonInit{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIView *contentView = [[UIView alloc]init];
    contentView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.navBar];
    [self.scrollView addSubview:contentView];
    [contentView addSubview:self.tipLabel];
    [contentView addSubview:self.codeImageView];
    [contentView addSubview:self.collectionView];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView);
        
    }];
    
    [self.codeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(contentView);
        make.top.equalTo(contentView.mas_top).offset(60);
    }];
    
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.codeImageView);
        make.centerY.equalTo(self.codeImageView.mas_bottom).offset(30);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLabel.mas_bottom).offset(40);
        make.left.right.equalTo(contentView);
        make.height.equalTo(@200);
        make.bottom.equalTo(contentView.mas_bottom);
    }];
}
#pragma mark - UICollectionView delegate & datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CCShareItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDAMShareItemCellIdentifier forIndexPath:indexPath];
    CCShareItemModel *itemModel = self.dataSource[indexPath.row];
    cell.itemModel = itemModel;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(SCREEN_WIDTH / 4, 90);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 0, 10, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    CCShareItemModel *shareImem = self.dataSource[indexPath.row];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:shareImem.action];
#pragma clang diagnostic pop
}

- (void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response {
    if (response.viewControllerType == UMSViewControllerOauth) {
        if (response.responseType == UMSViewControllerShareEdit && response.responseCode == UMSResponseCodeSuccess) {
            [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
        }else{
            [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
        }
    }
}
#pragma mark - Action
-(void)onBackButtonClicked:(UIButton *)backButton{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - getters and setters

-(UIImageView *)codeImageView{
    if (!_codeImageView) {
        _codeImageView = [[UIImageView alloc]init];
        _codeImageView.image = [UIImage imageNamed:@"qr_code"];
    }
    return _codeImageView;
}

-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.leftButton = self.backButton;
        _navBar.title = @"推荐给好友";
    }
    return _navBar;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(onBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}

-(UILabel *)tipLabel{
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.backgroundColor = [UIColor clearColor];
        _tipLabel.font = [UIFont systemFontOfSize:15];
        _tipLabel.text = @"扫描二维码可以直接下载CocoaChina";
    }
    return _tipLabel;
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.alwaysBounceVertical = YES;
    }
    return _scrollView;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[CCShareItemCell class] forCellWithReuseIdentifier:kDAMShareItemCellIdentifier];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

-(NSArray *)dataSource{
    if (!_dataSource) {
        CCShareItemModel *itemModel0 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"新浪微博",@"image":@"share_platform_sina",@"type":@0}];
        itemModel0.action = @selector(shareToSina);
        CCShareItemModel *itemModel1 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"微信好友",@"image":@"share_platform_wechat",@"type":@1}];
        itemModel1.action = @selector(shareToWeChat);
        CCShareItemModel *itemModel2 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"微信朋友圈",@"image":@"share_platform_wechattimeline",@"type":@2}];
        itemModel2.action = @selector(shareToTimeLine);
        CCShareItemModel *itemModel3 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"QQ好友",@"image":@"share_platform_qqfriends",@"type":@3}];
        itemModel3.action = @selector(shareToQQ);
        CCShareItemModel *itemModel4 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"QQ空间",@"image":@"share_platform_qzone",@"type":@4}];
        itemModel4.action = @selector(shareToQQZone);
        CCShareItemModel *itemModel5 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"用Safari打开",@"image":@"share_safari",@"type":@5}];
        itemModel5.action = @selector(openWithSafari);
        CCShareItemModel *itemModel6 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"拷贝链接",@"image":@"share_copy",@"type":@6}];
        itemModel6.action = @selector(copyLink);
        if([QQApiInterface isQQInstalled]){
            _dataSource = @[itemModel0,itemModel1,itemModel2,itemModel3,itemModel4,itemModel5,itemModel6];
        }else{
            _dataSource = @[itemModel0,itemModel1,itemModel2,itemModel5,itemModel6];
        }
    }
    return _dataSource;
}

#pragma mark - actions

-(void)openWithSafari{
    NSString *topicLink =@"http://cocoachina.bmob.cn";
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:topicLink]]) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:topicLink]];
    }
}

-(void)copyLink{
    NSString *topicLink =@"http://cocoachina.bmob.cn";
    [[UIPasteboard generalPasteboard] setPersistent:YES];
    [[UIPasteboard generalPasteboard] setValue:topicLink forPasteboardType:[UIPasteboardTypeListString objectAtIndex:0]];
    [JDStatusBarNotification showWithStatus:@"链接已拷贝" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
}

///新浪
-(void)shareToSina{
    NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
    NSString *title = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *topicLink =@"http://cocoachina.bmob.cn";
    NSString *sinaShareText = [NSString stringWithFormat:@"%@ %@", title, topicLink];
    [[UMSocialControllerService defaultControllerService] setShareText:sinaShareText shareImage:imageData socialUIDelegate:self];
    //设置分享内容和回调对象
    CCAppDelegate *appDel = [UIApplication sharedApplication].delegate;
    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina].snsClickHandler(appDel.rootViewController.topViewController,[UMSocialControllerService defaultControllerService],YES);
}

/// 微信好友
-(void)shareToWeChat{
    NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
    NSString *title = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *content = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *topicLink =@"http://cocoachina.bmob.cn";
    if ([WXApi isWXAppInstalled] == NO) {
        NSURL *url = [NSURL URLWithString:[WXApi getWXAppInstallUrl]];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
        return;
    }
    [UMSocialData defaultData].extConfig.wechatSessionData.wxMessageType = UMSocialWXMessageTypeNone;
    [UMSocialData defaultData].extConfig.wechatSessionData.url =  topicLink;
    [UMSocialData defaultData].extConfig.wechatSessionData.title = title;
    [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToWechatSession ] content:content image:imageData location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response) {
        if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
            [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
        }else{
            [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
        }
    }];
}

/// 微信朋友圈
-(void)shareToTimeLine{
    NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
    NSString *title = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *content = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *topicLink =@"http://cocoachina.bmob.cn";
    if ([WXApi isWXAppInstalled] == NO) {
        NSURL *url = [NSURL URLWithString:[WXApi getWXAppInstallUrl]];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
        return;
    }
    [UMSocialData defaultData].extConfig.wechatTimelineData.wxMessageType = UMSocialWXMessageTypeWeb;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = topicLink;
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;
    [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToWechatTimeline ] content:content image:imageData location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response) {
        if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
            [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
        }else{
            [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
        }
    }];
}

/// QQ好友
-(void)shareToQQ{
    NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
    NSString *title = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *content = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *topicLink =@"http://cocoachina.bmob.cn";
    [UMSocialData defaultData].extConfig.qqData.url = topicLink;
    [UMSocialData defaultData].extConfig.qqData.title = title;
    [UMSocialData defaultData].extConfig.qqData.qqMessageType = UMSocialQQMessageTypeDefault;
    [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToQQ ] content:content image:imageData location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response) {
        if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
            [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
        }else{
            [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
        }
    }];
}

/// QQ空间
-(void)shareToQQZone{
    NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
    NSString *title = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *content = @"最好用的CocoaChina客户端，实时掌握最快的技术快讯，CocoaChina让移动开发更简单";
    NSString *topicLink =@"http://cocoachina.bmob.cn";
    [UMSocialData defaultData].extConfig.qzoneData.url = topicLink;
    [UMSocialData defaultData].extConfig.qzoneData.title = title;
    [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToQzone ] content:content image:imageData location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response) {
        if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
            [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
        }else{
            [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [UMSocialControllerService defaultControllerService].socialUIDelegate = nil;
}

@end

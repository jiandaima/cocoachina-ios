//
//  YJLoginViewController.h
//  yuedu
//
//  Created by Zhang on 15/9/23.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCLoginViewController : UIViewController

@property (nonatomic, strong) Class redirectViewController;

@end

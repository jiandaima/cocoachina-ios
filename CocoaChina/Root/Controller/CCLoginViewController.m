//
//  YJLoginViewController.m
//  yuedu
//
//  Created by Zhang on 15/9/23.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCLoginViewController.h"
#import "CCNavigationBar.h"
#import "CCLoginCollectionViewCell.h"
#import <UMengSocial/UMSocial.h>
#import "CCRegistViewController.h"

NSString *const kYJLoginCollectionViewCellIdentifier = @"kYJLoginCollectionViewCellIdentifier";

@interface CCLoginViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UIView *contentView;
///登录
@property (nonatomic, strong) UIView *topContentView;
@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) UIImageView *passwdImageView;
@property (nonatomic, strong) UITextField *usernameField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UIView *userLine;
@property (nonatomic, strong) UIView *passwordLine;
@property (nonatomic, strong) UIButton *forgetButton;
@property (nonatomic, strong) UIButton *reigstButton;
@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UILabel *otherLoginLabel;
@property (nonatomic, strong) UIView *bottomLine;

//第三方登录
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *images;

@property (nonatomic, strong) AFHTTPRequestOperation *operation;
@end

@implementation CCLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self commonInit];
}

-(void)commonInit{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.mainScrollView];
    [self.view addSubview:self.navBar];
    [self.mainScrollView addSubview:self.contentView];
    [self.contentView addSubview:self.topContentView];
    [self.contentView addSubview:self.collectionView];
    //用户名
    [self.topContentView addSubview:self.userImageView];
    [self.topContentView addSubview:self.usernameField];
    [self.topContentView addSubview:self.userLine];
    //密码
    [self.topContentView addSubview:self.passwdImageView];
    [self.topContentView addSubview:self.passwordTextField];
    [self.topContentView addSubview:self.passwordLine];
    [self.topContentView addSubview:self.loginButton];
    
    [self.topContentView addSubview:self.forgetButton];
    [self.topContentView addSubview:self.reigstButton];
    [self.topContentView addSubview:self.otherLoginLabel];
    [self.topContentView addSubview:self.bottomLine];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    [self.mainScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.bottom.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.mainScrollView);
        make.width.equalTo(self.mainScrollView);
    }];
    
    [self.topContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
    }];
    
    [self.userImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topContentView.mas_top).offset(40);
        make.left.equalTo(self.topContentView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.usernameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.userImageView);
        make.left.equalTo(self.userImageView.mas_right).offset(8);
        make.right.equalTo(self.topContentView.mas_right).offset(-15);
        make.height.equalTo(@30);
    }];
    
    [self.userLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.topContentView.mas_left).offset(15);
        make.right.equalTo(self.topContentView.mas_right).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.userImageView.mas_bottom).offset(10);
    }];
    
    [self.passwdImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userLine.mas_bottom).offset(15);
        make.left.equalTo(self.topContentView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.passwdImageView);
        make.left.equalTo(self.passwdImageView.mas_right).offset(8);
        make.right.equalTo(self.topContentView.mas_right).offset(-15);
        make.height.equalTo(@30);
    }];
    
    [self.passwordLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.topContentView.mas_left).offset(15);
        make.right.equalTo(self.topContentView.mas_right).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.passwdImageView.mas_bottom).offset(10);
    }];
    
    [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordLine.mas_bottom).offset(20);
        make.left.equalTo(self.topContentView.mas_left).offset(15);
        make.right.equalTo(self.topContentView.mas_right).offset(-15);
        make.height.equalTo(@44);
    }];
    
    [self.forgetButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.loginButton.mas_bottom).offset(15);
        make.left.equalTo(self.loginButton.mas_left);
    }];
    
    [self.reigstButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.loginButton.mas_bottom).offset(15);
        make.right.equalTo(self.loginButton.mas_right);
    }];
    
    [self.otherLoginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.forgetButton.mas_bottom).offset(15);
        make.left.equalTo(self.topContentView.mas_left).offset(15);
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.otherLoginLabel.mas_bottom).offset(15);
        make.left.right.equalTo(self.topContentView);
        make.height.equalTo(@0.5);
        make.bottom.equalTo(self.topContentView);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topContentView.mas_bottom);
        make.left.right.equalTo(self.contentView);
        make.height.equalTo(@(SCREEN_WIDTH/3));
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.isDragging) {
        [self.view endEditing:YES];
    }
}

#pragma mark - collectionView dataSource Or delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 3;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CCLoginCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kYJLoginCollectionViewCellIdentifier forIndexPath:indexPath];
    cell.title = self.titles[indexPath.row];
    cell.image = self.images[indexPath.row];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
                NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            }
        });
    }
    
    if (indexPath.row == 1) {
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            //          获取微博用户名、uid、token等
            if (response.responseCode == UMSResponseCodeSuccess) {
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
                NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            }});
    }
    
    if (indexPath.row == 2) {
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            //          获取微博用户名、uid、token等
            if (response.responseCode == UMSResponseCodeSuccess) {
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQQ];
                NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                [self snsLogin:@{@"username":snsAccount.userName,@"accessToken":snsAccount.accessToken,@"snsType":@(1)}];
                
            }});
    }
}

-(NSArray *)titles{
    if (!_titles) {
        _titles = @[@"微信账号登录",@"新浪微博登录",@"QQ账号登录"];
    }
    return _titles;
}

-(NSArray *)images{
    if (!_images) {
        _images = @[[UIImage imageNamed:@"share_platform_wechat"],
                    [UIImage imageNamed:@"share_platform_sina"],
                    [UIImage imageNamed:@"share_platform_qqfriends"]];
    }
    return _images;
}


#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.usernameField) {
        [self.passwordTextField becomeFirstResponder];
    }
    if (textField == self.passwordTextField) {
        [self.usernameField becomeFirstResponder];
    }
    return YES;
}

#pragma mark - actions
-(void)onBackButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onRegistButtonClicked:(UIButton *)sender{
    CCRegistViewController *registViewController = [[CCRegistViewController alloc]init];
    [self.navigationController pushViewController:registViewController animated:YES];
}

-(void)onForgetButtonClicked:(UIButton *)sender{
    
}

-(void)onLoginButtonClicked:(UIButton *)sender{
    
    [self login];
}

#pragma mark - request

-(void)login{
    NSString *username = self.usernameField.text;
    NSString *password = self.passwordTextField.text;
    [SVProgressHUD showWithStatus:@"登录中..."];
  
    
}

-(void)snsLogin:(NSDictionary *)parameters{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]initWithDictionary:parameters];
    param[@"phone"] = @"18513583007";
    param[@"code"] = @"1234";
    param[@"password"] = @"sdfsf";
    
}

#pragma mark - getters and setters
-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.leftButton = self.backButton;
        _navBar.title = @"登录CocoaChina";
    }
    return _navBar;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(onBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}


-(UIScrollView *)mainScrollView{
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc]init];
        _mainScrollView.alwaysBounceVertical = YES;
        _mainScrollView.delegate = self;
    }
    return _mainScrollView;
}

-(UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
        [_collectionView registerClass:[CCLoginCollectionViewCell class] forCellWithReuseIdentifier:kYJLoginCollectionViewCellIdentifier];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor yj_cellBackgroundColor];
    }
    return _collectionView;
}

-(UICollectionViewFlowLayout *)flowLayout{
    if (!_flowLayout) {
        _flowLayout = [[UICollectionViewFlowLayout alloc]init];
        [_flowLayout setItemSize:CGSizeMake(floor(SCREEN_WIDTH/3.f), SCREEN_WIDTH/3.f)];
        [_flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        _flowLayout.minimumInteritemSpacing = 0;
        _flowLayout.minimumLineSpacing = 0;
    }
    return _flowLayout;
}

-(UIView *)topContentView{
    if (!_topContentView) {
        _topContentView = [[UIView alloc]init];
        _topContentView.backgroundColor = [UIColor whiteColor];
    }
    return _topContentView;
}

-(UIImageView *)userImageView{
    if (!_userImageView) {
        _userImageView = [[UIImageView alloc]init];
        _userImageView.image = [UIImage imageNamed:@"login_username_icon"];
    }
    return _userImageView;
}
-(UIImageView *)passwdImageView{
    if (!_passwdImageView) {
        _passwdImageView = [[UIImageView alloc]init];
        _passwdImageView.image = [UIImage imageNamed:@"login_password_icon"];
    }
    return _passwdImageView;
}

-(UITextField *)usernameField{
    if (!_usernameField) {
        _usernameField = [[UITextField alloc]init];
        _usernameField.returnKeyType = UIReturnKeyNext;
        _usernameField.font = [UIFont systemFontOfSize:15];
        _usernameField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"手机号" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor]}];
        _usernameField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _usernameField.delegate = self;
        _usernameField.keyboardType = UIKeyboardTypePhonePad;
    }
    return _usernameField;
}

-(UITextField *)passwordTextField{
    if (!_passwordTextField) {
        _passwordTextField = [[UITextField alloc]init];
        _passwordTextField.returnKeyType = UIReturnKeyNext;
        _passwordTextField.font = [UIFont systemFontOfSize:15];
        _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"密码" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor]}];
        _passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _passwordTextField.secureTextEntry = YES;
        _passwordTextField.delegate = self;
    }
    return _passwordTextField;
}

-(UIView *)userLine{
    if (!_userLine) {
        _userLine = [[UIView alloc]init];
        _userLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _userLine;
}

-(UIView *)passwordLine{
    if (!_passwordLine) {
        _passwordLine = [[UIView alloc]init];
        _passwordLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _passwordLine;
}

-(UIButton *)loginButton{
    if (!_loginButton) {
        _loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:18];
        [_loginButton addTarget:self action:@selector(onLoginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_loginButton setTitle:@"登 录" forState:UIControlStateNormal];
        [_loginButton setTitleColor:[UIColor yj_darkTextColor] forState:UIControlStateNormal];
        _loginButton.layer.cornerRadius = 5;
        _loginButton.layer.borderWidth = 0.5;
        _loginButton.layer.borderColor = [UIColor yj_lineColor].CGColor;
        _loginButton.layer.masksToBounds = YES;
        [_loginButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [_loginButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_grayColor]] forState:UIControlStateHighlighted];
    }
    return _loginButton;
}

-(UIButton *)forgetButton{
    if (!_forgetButton) {
        _forgetButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_forgetButton addTarget:self action:@selector(onForgetButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_forgetButton setTitle:@" 忘记密码? " forState:UIControlStateNormal];
        _forgetButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_forgetButton setTitleColor:[UIColor yj_lightTextColor] forState:UIControlStateNormal];
        _forgetButton.layer.borderColor = [UIColor yj_lineColor].CGColor;
        _forgetButton.layer.borderWidth = 0.5;
        _forgetButton.layer.cornerRadius = 5;
        _forgetButton.layer.masksToBounds = YES;
        [_forgetButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [_forgetButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_grayColor]] forState:UIControlStateHighlighted];
    }
    return _forgetButton;
}

-(UIButton *)reigstButton{
    if (!_reigstButton) {
        _reigstButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_reigstButton setTitle:@" 手机号快速注册 > " forState:UIControlStateNormal];
        [_reigstButton addTarget:self action:@selector(onRegistButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _reigstButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_reigstButton setTitleColor:[UIColor yj_lightTextColor] forState:UIControlStateNormal];
        _reigstButton.layer.borderColor = [UIColor yj_lineColor].CGColor;
        _reigstButton.layer.borderWidth = 0.5;
        _reigstButton.layer.cornerRadius = 5;
        _reigstButton.layer.masksToBounds = YES;
        [_reigstButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [_reigstButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_grayColor]] forState:UIControlStateHighlighted];
    }
    return _reigstButton;
}

-(UILabel *)otherLoginLabel{
    if (!_otherLoginLabel) {
        _otherLoginLabel = [[UILabel alloc]init];
        _otherLoginLabel.text = @"还可以选择以下登录方式";
        _otherLoginLabel.font = [UIFont systemFontOfSize:13];
        _otherLoginLabel.textColor = [UIColor yj_lightTextColor];
    }
    return _otherLoginLabel;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _bottomLine;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)dealloc{
    [self.operation cancel];
}

@end

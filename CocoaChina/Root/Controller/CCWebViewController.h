//
//  CCWebViewController.h
//  CocoaChina
//
//  Created by Zhang on 15/10/29.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMasterModel.h"

@interface CCWebViewController : UIViewController
@property (nonatomic, strong) CCMasterModel *masterModel;
@end

//
//  CCMasterViewController.m
//  CocoaChina
//
//  Created by Zhang on 15/10/30.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCMasterViewController.h"
#import <WebKit/WebKit.h>
#import "CCNavigationBar.h"
#import "CCMasterTableViewCell.h"
#import "UIScrollView+EmptyDataSet.h"
#import "CCWebViewController.h"
#import "MJRefresh.h"
#import "YJHistroyService.h"
#import "CCMasterModel.h"

@interface CCMasterViewController ()<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,UIViewControllerPreviewingDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign,getter = isRefreshing) BOOL refreshing;
@property (nonatomic, assign) NSInteger limit;
@property (nonatomic, assign) NSInteger skip;
@end

@implementation CCMasterViewController

-(instancetype)initWithChannelModel:(CCChannelModel *)channelModel{
    self = [super init];
    if (self) {
        _channelModel = channelModel;
        _limit = 20;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self commonInit];
    
    __weak typeof(self) weakSelf = self;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.skip = 0;
        weakSelf.refreshing = YES;
        [weakSelf getNewData];
    }];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:13];
    header.lastUpdatedTimeLabel.textColor = [UIColor yj_lightTextColor];
    header.stateLabel.font = [UIFont systemFontOfSize:13];
    header.stateLabel.textColor = [UIColor yj_lightTextColor];
    header.lastUpdatedTimeKey = [NSString stringWithFormat:@"%@%@",self.channelModel.channelName,self.channelModel.channelType];
    self.tableView.mj_header = header;
    
    MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
        weakSelf.skip +=self.limit;
        [weakSelf getNewData];
    }];
    footer.triggerAutomaticallyRefreshPercent = -20;
    footer.stateLabel.font = [UIFont systemFontOfSize:13];
    footer.stateLabel.textColor = [UIColor yj_lightTextColor];
    [footer setTitle:@"没有更多了" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    [self loadCache];
    ///Regist 3D Touch
    if (IOS_9_OR_LATER) {
        if (self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable) {
            [self registerForPreviewingWithDelegate:self sourceView:self.tableView];
        }
    }
}

- (nullable UIViewController *)previewingContext:(id <UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location NS_AVAILABLE_IOS(9_0){
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    CCMasterTableViewCell *cell = (CCMasterTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    if(!cell){
        return nil;
    }
    previewingContext.sourceRect = cell.frame;
    CCMasterModel *masterCellModel = self.dataSource[indexPath.row];
    CCWebViewController *webViewController = [[CCWebViewController alloc]init];
    webViewController.masterModel = masterCellModel;
    return webViewController;
}

- (void)previewingContext:(id <UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit NS_AVAILABLE_IOS(9_0){
    CGPoint location = [previewingContext.previewingGestureRecognizerForFailureRelationship locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    CCMasterTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    previewingContext.sourceRect = cell.frame;
    CCMasterModel *masterCellModel = self.dataSource[indexPath.row];
    [[YJHistroyService sharedYJHistroyService]addHistrory:masterCellModel];
    masterCellModel.read = YES;
    cell.model = masterCellModel;
    [self showViewController:viewControllerToCommit sender:self];
}

-(void)commonInit{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - overwrite
-(void)setFirstLoad:(BOOL)firstLoad{
    if (!_firstLoad) {
        [self.tableView.mj_header beginRefreshing];
    }
    _firstLoad = YES;
}

#pragma mark - WKScriptMessageHandler

-(BmobQuery *)queryWithCachePolicy:(BmobCachePolicy)cachePolicy{
    BmobQuery *query = [BmobQuery queryWithClassName:@"newslist"];
    query.cachePolicy = cachePolicy;
    return query;
}

-(void)getNewData{
    [self getDataCachePolicy:kBmobCachePolicyNetworkOnly];
}

-(void)loadCache{
    self.skip = 0;
    [self getDataCachePolicy:kBmobCachePolicyCacheOnly];
}

-(void)getDataCachePolicy:(BmobCachePolicy)cachePolicy{
    __weak CCMasterViewController *weakSelf = self;
    BmobQuery *query = [BmobQuery queryWithClassName:@"newslist"];
    query.cachePolicy = cachePolicy;
    query.limit = self.limit;
    query.skip = self.skip;//self.dataSource.count;
    query.maxCacheAge = 60*60*24*7;
    query.isGroupcount = YES;
    [query orderByDescending:@"newsCreateTime"];
    [query whereKey:@"newsType" equalTo:self.channelModel.channelType];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array, NSError *error) {

        if (weakSelf.isRefreshing && !error) {
            [weakSelf.dataSource removeAllObjects];
        }
        for (BmobObject *masterObject in array) {
            NSDictionary *dic = [masterObject valueForKey:@"bmobDataDic"];
            CCMasterModel *masterModel = [[CCMasterModel alloc]initWithDictionary:dic];
            masterModel.read = [[YJHistroyService sharedYJHistroyService]isInHistroy:masterModel];
            if (![weakSelf.dataSource containsObject:masterModel]) {
                [weakSelf.dataSource addObject:masterModel];
            }
        }
        if (cachePolicy != kBmobCachePolicyCacheOnly) {
            if (weakSelf.isRefreshing) {
                [weakSelf.tableView.mj_header endRefreshing];
                weakSelf.refreshing = NO;
            }else{
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            if (array.count < weakSelf.limit) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        [weakSelf.tableView reloadData];
    }];
}


#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CCMasterModel *masterCellModel = self.dataSource[indexPath.row];
    [[YJHistroyService sharedYJHistroyService]addHistrory:masterCellModel];
    masterCellModel.read = YES;
    CCMasterTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.model = masterCellModel;
    
    CCWebViewController *webViewController = [[CCWebViewController alloc]init];
    webViewController.masterModel = masterCellModel;
    [self showViewController:webViewController sender:self];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCMasterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMasterCellIdentifier];
    cell.model = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark - DZNEmptyDataSetDelegate
-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return self.dataSource == 0;
}

-(NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    return [[NSAttributedString alloc]initWithString:@"您还没有阅读过任何文章" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
}

-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    return [UIImage imageNamed:@""];
}

#pragma mark - override
-(void)setScrollToTop:(BOOL)scrollToTop{
    self.tableView.scrollsToTop = scrollToTop;
}

- (BOOL)isEqual:(CCMasterViewController *)other{
    if (other == self) {
        return YES;
    } else if (![super isEqual:other]) {
        return NO;
    } else {
        return  [other.channelModel.channelId isEqualToString:self.channelModel.channelId];;
    }
}

- (NSUInteger)hash{
    return [self.channelModel.channelId hash];
}

#pragma mark - getters and setters

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor yj_cellBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.emptyDataSetDelegate = self;
        _tableView.emptyDataSetSource = self;
        _tableView.rowHeight = 95;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CCMasterTableViewCell class] forCellReuseIdentifier:kMasterCellIdentifier];
    }
    return _tableView;
}

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc]init];
    }
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
//
//  YJHistroyService.h
//  yuedu
//
//  Created by Zhang on 15/10/21.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCHistoryModel.h"
#import "YJSingleton.h"
#import "CCMasterModel.h"

extern NSString *const YJClearReadHistoryNotification;

@interface YJHistroyService : NSObject
singleton_interface(YJHistroyService)

///添加于都历史记录
-(void)addHistrory:(CCMasterModel *)weChatModel;
///删除阅读历史记录
-(void)removeHistroy:(CCMasterModel *)weChatModel;
///是否存在历史记录
-(BOOL)isInHistroy:(CCMasterModel *)newsModel;
///通过newsId获取历史记录
-(CCHistoryModel *)getHistroyByNewsId:(NSString *)newsId;
///获取历史记录列表
-(NSArray *)historyListByOffset:(NSInteger)offset limit:(NSInteger)limit;

@end

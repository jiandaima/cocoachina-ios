//
//  CCFavoriteService.m
//  CocoaChina
//
//  Created by Zhang on 15/10/31.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCFavoriteService.h"
#import "CCDBManager.h"
#import "NSDictionary+YJJsonString.h"
#import "CCHistoryModel.h"
#import "CCMasterModel.h"

@implementation CCFavoriteService

singleton_implementation(CCFavoriteService)

///添加于都历史记录
-(void)addFavorite:(CCMasterModel *)weChatModel{
    NSString *sql = @"REPLACE INTO favorite_table (newsId,readdate,data) VALUES(:newsId,:readdate,:data)";
    NSTimeInterval interval = [[NSDate date]timeIntervalSince1970];
    NSDictionary *parameters = @{@"newsId":weChatModel.newsId,@"readdate":@(interval),@"data":[weChatModel.initalDictionary toJSONString]};
    [[CCDBManager sharedCCDBManager] executeNonQuery:sql parameterDictionary:parameters];
}

///删除阅读历史记录
-(void)removeFavorite:(CCMasterModel *)weChatModel{
    NSString *sql = @"DELETE FROM favorite_table WHERE newsId = :newsId";
    NSDictionary *parameters = @{@"newsId":weChatModel.newsId};
    [[CCDBManager sharedCCDBManager] executeNonQuery:sql parameterDictionary:parameters];
}

///是否存在历史记录
-(BOOL)isInFavorite:(CCMasterModel *)weChatModel{
    NSString *sql = @"SELECT * FROM favorite_table WHERE newsId = :newsId";
    NSDictionary *parameters = @{@"newsId":weChatModel.newsId};
    NSArray *result = [[CCDBManager sharedCCDBManager] executeQuery:sql parameterDictionary:parameters];
    return result.count > 0;
}

///通过newsId获取历史记录
-(CCHistoryModel *)getFavoriteByNewsId:(NSString *)newsId{
    NSString *sql = @"SELECT * FROM favorite_table WHERE newsId = :newsId";
    NSDictionary *parameters = @{@"newsId":newsId};
    NSArray *result = [[CCDBManager sharedCCDBManager] executeQuery:sql parameterDictionary:parameters];
    NSDictionary *favoriteDictionary = (NSDictionary *)[result firstObject];
    if (result) {
        CCHistoryModel *favoriteModel = [[CCHistoryModel alloc]initWithDictionary:favoriteDictionary];
        return favoriteModel;
    }
    return nil;
}

-(void)clearHistroy{
    NSString *sql = @"DROP TABLE favorite_table";
    [[CCDBManager sharedCCDBManager] executeNonQuery:sql parameterDictionary:nil];
}

///获取历史记录列表
-(NSArray *)favoriteListByOffset:(NSInteger)offset limit:(NSInteger)limit{
    
    NSString *sql=@"SELECT * FROM favorite_table ORDER BY readdate DESC LIMIT :limit OFFSET :offset";
    NSDictionary *parameters = @{@"limit":@(limit),@"offset":@(offset)};
    NSArray *result = [[CCDBManager sharedCCDBManager] executeQuery:sql parameterDictionary:parameters];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (NSDictionary *favoriteDictionary in result) {
        CCHistoryModel *favoriteModel = [[CCHistoryModel alloc]initWithDictionary:favoriteDictionary];
        [array addObject:favoriteModel];
    }
    return [array copy];
}

@end

//
//  YJReadHistoryModel.h
//  yuedu
//
//  Created by Zhang on 15/10/21.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCMasterModel.h"

@interface CCHistoryModel:CCObject

@property (nonatomic, strong) NSString *newsId;

@property (nonatomic, strong) CCMasterModel *masterCellModel;

@property (nonatomic, assign) NSTimeInterval readdate;

@end

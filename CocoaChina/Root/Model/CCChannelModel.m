//
//  YJChannelModel.m
//  yuedu
//
//  Created by Zhang on 15/9/20.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCChannelModel.h"

@implementation CCChannelModel

//===========================================================
//  Keyed Archiving
//
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.channelName forKey:@"channelName"];
    [encoder encodeObject:self.channelId forKey:@"channelId"];
    [encoder encodeObject:self.channelType forKey:@"channelType"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.channelName = [decoder decodeObjectForKey:@"channelName"];
        self.channelId = [decoder decodeObjectForKey:@"channelId"];
        self.channelType = [decoder decodeObjectForKey:@"channelType"];
    }
    return self;
}

- (BOOL)isEqual:(CCChannelModel *)other{
    if (other == self) {
        return YES;
    } else {
        return [self.channelId isEqualToString:other.channelId];
    }
}

- (NSUInteger)hash{
    return [self.channelId hash];
}

@end

//
//  CCMasterModel.h
//  CocoaChina
//
//  Created by Zhang on 15/11/7.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCObject.h"

@interface CCMasterModel : CCObject
@property (nonatomic, strong) NSString *newsTitle;
@property (nonatomic, strong) NSString *newsImage;
@property (nonatomic, strong) NSString *newsLink;
@property (nonatomic, strong) NSString *newsType;
@property (nonatomic, strong) NSString *newsTypeName;
@property (nonatomic, assign) NSInteger newsNum;
@property (nonatomic, strong) NSString *newsSource;
@property (nonatomic, strong) NSString *newsCreateTime;
@property (nonatomic, strong) NSString *newsId;
@property (nonatomic, assign,getter=isRead) BOOL read;
@property (nonatomic, copy) NSString *objectId;
@end

//
//  YJChannelModel.h
//  yuedu
//
//  Created by Zhang on 15/9/20.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCObject.h"

@interface CCChannelModel : CCObject

@property (nonatomic, copy) NSString *channelName;

@property (nonatomic, copy) NSString *channelId;

@property (nonatomic, strong) NSString *channelType;

@end

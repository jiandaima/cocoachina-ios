//
//  YJChannelManager.m
//  yuedu
//
//  Created by Zhang on 15/9/20.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCChannelManager.h"
#import "CCChannelModel.h"

@interface CCChannelManager ()
@property (nonatomic, strong) NSString *filePath;
@end

@implementation CCChannelManager

+(instancetype)sharedManager{
    static CCChannelManager *_shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareManager  = [[CCChannelManager alloc]init];
    });
    return _shareManager;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        _confirmedChannels = [[NSMutableArray alloc]init];
        [self loadData];
    }
    return self;
}

-(NSString *)filePath{
    if (!_filePath) {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        _filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"channels"];
        if (![fileMgr fileExistsAtPath:_filePath]) {
            NSString *subjectFilePath = [[NSBundle mainBundle]pathForResource:@"channels" ofType:@"json"];
            NSData *subjectData = [NSData dataWithContentsOfFile:subjectFilePath];
            NSDictionary *subjectDictionry =[NSJSONSerialization JSONObjectWithData:subjectData options:NSJSONReadingAllowFragments error:nil];
            if (subjectDictionry) {
                NSMutableArray *channelArray = [[NSMutableArray alloc]init];
                NSArray *channels = [[NSArray alloc]initWithArray:subjectDictionry[@"channels"]];
                for (NSDictionary *channelDictionary in channels) {
                    CCChannelModel *channelModel = [[CCChannelModel alloc]initWithDictionary:channelDictionary];
                    if (![channelArray containsObject:channelModel]) {
                        [channelArray addObject:channelModel];
                    }
                }
                NSDictionary *dic = @{@"channels":channelArray};
                [NSKeyedArchiver archiveRootObject:dic toFile:_filePath];
            }
        }
    }
    return _filePath;
}

-(void)reloadData{
    [self loadData];
}

-(void)loadData{
    NSDictionary *channelsDictionry = [NSKeyedUnarchiver unarchiveObjectWithFile:[self filePath]];
    self.confirmedChannels = [NSMutableArray arrayWithArray:channelsDictionry[@"channels"]];
}

-(void)save{
    NSDictionary *dic = @{@"confirmed":self.confirmedChannels};
    [NSKeyedArchiver archiveRootObject:dic toFile:[self filePath]];
}

@end

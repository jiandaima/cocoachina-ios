//
//  CCRefreshHeader.h
//  CocoaChina
//
//  Created by Zhang on 15/11/4.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import <MJRefresh/MJRefreshStateHeader.h>

@interface CCRefreshHeader : MJRefreshStateHeader
/** 设置state状态下的动画图片images 动画持续时间duration*/
- (void)setImages:(NSArray *)images duration:(NSTimeInterval)duration forState:(MJRefreshState)state;
- (void)setImages:(NSArray *)images forState:(MJRefreshState)state;@end

//
//  YJChannelManner.h
//  yuedu
//
//  Created by Zhang on 15/9/19.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCChannelManner : NSObject

+(instancetype)sharedManager;

@end

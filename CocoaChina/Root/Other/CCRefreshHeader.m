//
//  CCRefreshHeader.m
//  CocoaChina
//
//  Created by Zhang on 15/11/4.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCRefreshHeader.h"

@interface CCRefreshHeader()
@property (weak, nonatomic) UIImageView *gifView;
/** 所有状态对应的动画图片 */
@property (strong, nonatomic) NSMutableDictionary *stateImages;
/** 所有状态对应的动画时间 */
@property (strong, nonatomic) NSMutableDictionary *stateDurations;
@end

@implementation CCRefreshHeader
#pragma mark - 懒加载
- (UIImageView *)gifView
{
    if (!_gifView) {
        UIImageView *gifView = [[UIImageView alloc] init];
        [self addSubview:_gifView = gifView];
        [_gifView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX).offset(-50);
            make.centerY.equalTo(self.mas_bottom).offset(-30);
        }];
    }
    return _gifView;
}

- (NSMutableDictionary *)stateImages
{
    if (!_stateImages) {
        self.stateImages = [NSMutableDictionary dictionary];
    }
    return _stateImages;
}

- (NSMutableDictionary *)stateDurations
{
    if (!_stateDurations) {
        self.stateDurations = [NSMutableDictionary dictionary];
    }
    return _stateDurations;
}

#pragma mark - 公共方法
- (void)setImages:(NSArray *)images duration:(NSTimeInterval)duration forState:(MJRefreshState)state
{
    if (images == nil) return;
    
    self.stateImages[@(state)] = images;
    self.stateDurations[@(state)] = @(duration);
    
    /* 根据图片设置控件的高度 */
    UIImage *image = [images firstObject];
    if (image.size.height > self.mj_h) {
        self.mj_h = image.size.height;
    }
}

- (void)setImages:(NSArray *)images forState:(MJRefreshState)state
{
    [self setImages:images duration:images.count * 0.1 forState:state];
}

#pragma mark - 实现父类的方法
- (void)setPullingPercent:(CGFloat)pullingPercent
{
    [super setPullingPercent:pullingPercent];
    NSArray *images = self.stateImages[@(MJRefreshStateIdle)];
    if (self.state == MJRefreshStateIdle) {
        if (pullingPercent>0) {
            pullingPercent = MAX(0, pullingPercent);
            pullingPercent = MIN(1, pullingPercent);
            self.gifView.transform =  CGAffineTransformScale(CGAffineTransformIdentity, pullingPercent, pullingPercent);
            NSLog(@"%@",@(pullingPercent));
        }
    }else{
        self.gifView.transform = CGAffineTransformIdentity;
    }
    
    if (self.state != MJRefreshStateIdle || images.count == 0) return;
    // 停止动画
    [self.gifView stopAnimating];
    // 设置当前需要显示的图片
    NSUInteger index =  images.count * pullingPercent;
    if (index >= images.count) index = images.count - 1;
    self.gifView.image = images[index];
}

- (void)placeSubviews
{
    [super placeSubviews];
    
    // 状态
    self.stateLabel.textAlignment = NSTextAlignmentLeft;
    self.stateLabel.mj_x = self.mj_w * 0.5-10;
    self.stateLabel.mj_y = 0;
    self.stateLabel.mj_w = self.mj_w * 0.5;
    self.stateLabel.mj_h = self.mj_h * 0.5;
    
    // 更新时间
    self.lastUpdatedTimeLabel.textAlignment = NSTextAlignmentLeft;
    self.lastUpdatedTimeLabel.mj_x = self.mj_w * 0.5-10;
    self.lastUpdatedTimeLabel.mj_y = self.stateLabel.mj_h-5;
    self.lastUpdatedTimeLabel.mj_w = self.mj_w*0.5;
    self.lastUpdatedTimeLabel.mj_h = self.mj_h - self.lastUpdatedTimeLabel.mj_y;   
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState
    
    // 根据状态做事情
    if (state == MJRefreshStatePulling || state == MJRefreshStateRefreshing) {
        NSArray *images = self.stateImages[@(state)];
        if (images.count == 0) return;
        
        [self.gifView stopAnimating];
        if (images.count == 1) { // 单张图片
            self.gifView.image = [images lastObject];
        } else { // 多张图片
            self.gifView.animationImages = images;
            self.gifView.animationDuration = [self.stateDurations[@(state)] doubleValue];
            [self.gifView startAnimating];
        }
    }
}
@end


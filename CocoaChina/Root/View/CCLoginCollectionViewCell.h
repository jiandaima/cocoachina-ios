//
//  YJLoginCollectionViewCell.h
//  yuedu
//
//  Created by Zhang on 15/10/13.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCLoginCollectionViewCell : UICollectionViewCell
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UIImage *image;
@end

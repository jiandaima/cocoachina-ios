//
//  YJChannelView.h
//  yuedu
//
//  Created by Zhang on 15/9/19.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMChannelViewDataSource;
@protocol DAMChannelViewDelegate;
@interface CCChannelView : UIView

@property (nonatomic, weak) id<DAMChannelViewDataSource> dataSource;
@property (nonatomic, weak) id<DAMChannelViewDelegate> delegate;

- (void)channelViewDidScroll:(UIScrollView *)scrollView;

- (void)channelViewDidEndDecelerating:(UIScrollView *)scrollView;

- (void)reloadData;

@end

@protocol DAMChannelViewDataSource <NSObject>
@required
- (NSInteger)numberOfChannelsForChannelView:(CCChannelView *)channelView;

- (NSString *)titleForChannelForChannelView:(CCChannelView *)channelView atIndex:(NSInteger)index;

@end

@protocol DAMChannelViewDelegate <NSObject>

-(void)channelView:(CCChannelView *)channelView didSelectedItemAtIndex:(NSInteger)index;


@end
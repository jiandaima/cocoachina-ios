//
//  CCMasterTableViewCell.m
//  CocoaChina
//
//  Created by Zhang on 15/10/30.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCMasterTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CCMasterModel.h"

NSString *const kMasterCellIdentifier = @"kMasterCellIdentifier";

@interface CCMasterTableViewCell ()
///选中样式
@property (nonatomic, strong) UIView *selectedBG;
///内容图片
@property (nonatomic, strong) UIImageView *contentImageView;
///标题
@property (nonatomic, strong) UILabel *titleLabel;
///作者
@property (nonatomic, strong) UILabel *userNameLabel;
///底部分割线
@property (nonatomic, strong) UIView *bottomLine;
@property (nonatomic, strong) MASConstraint *markLabelWidthConstraint;
@property (nonatomic, strong) MASConstraint *markLabelHeightConstraint;
@end

@implementation CCMasterTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.contentView.backgroundColor = [UIColor yj_cellBackgroundColor];
    self.backgroundColor = [UIColor yj_cellBackgroundColor];
    self.selectedBackgroundView = self.selectedBG;
    [self.contentView addSubview:self.contentImageView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.bottomLine];
    [self.contentView addSubview:self.userNameLabel];
    
    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(100, 75));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentImageView.mas_top);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentImageView.mas_left).offset(-10);
    }];
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentImageView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentImageView.mas_left).offset(-10);
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView).insets(UIEdgeInsetsMake(0, 10, 0, 10));
        make.height.equalTo(@0.5);
    }];
}

#pragma mark - datasource
-(void)setModel:(CCMasterModel *)model{
    _model = model;
    
    [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:model.newsImage] placeholderImage:[UIImage imageNamed:@"image_placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
   self.titleLabel.textColor = model.isRead ? [UIColor yj_lightTextColor]:[UIColor yj_darkTextColor];
    self.titleLabel.text = model.newsTitle;
    self.userNameLabel.text = [NSString stringWithFormat:@"%@ · %@",model.newsCreateTime,model.newsSource];
}

#pragma mark - getters and setters
-(UIView *)selectedBG{
    if (!_selectedBG) {
        _selectedBG = [[UIView alloc]init];
        _selectedBG.backgroundColor = [UIColor yj_cellSelectedColor];
    }
    return _selectedBG;
}

-(UIImageView *)contentImageView{
    if (!_contentImageView) {
        _contentImageView = [[UIImageView alloc]init];
        _contentImageView.contentMode = UIViewContentModeScaleAspectFill;
        _contentImageView.layer.borderWidth = 0.5;
        _contentImageView.layer.borderColor = [UIColor yj_lineColor].CGColor;
        _contentImageView.layer.cornerRadius = 3;
        _contentImageView.layer.masksToBounds = YES;
        _contentImageView.layer.allowsEdgeAntialiasing = YES;
    }
    return _contentImageView;
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.numberOfLines = 3;
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _titleLabel.textColor = [UIColor yj_darkTextColor];
    }
    return _titleLabel;
}

-(UILabel *)userNameLabel{
    if (!_userNameLabel) {
        _userNameLabel = [[UILabel alloc]init];
        _userNameLabel.backgroundColor = [UIColor clearColor];
        _userNameLabel.font = [UIFont systemFontOfSize:12];
        _userNameLabel.textColor = [UIColor yj_lightTextColor];
    }
    return _userNameLabel;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _bottomLine;
}

@end

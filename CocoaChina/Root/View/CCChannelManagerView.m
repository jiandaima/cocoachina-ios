//
//  YJChannelManagerView.m
//  yuedu
//
//  Created by Zhang on 15/9/22.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCChannelManagerView.h"
#import "CCChannelManager.h"


@interface CCChannelManagerView ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic,strong) UICollectionView *collectionView;

@end

@implementation CCChannelManagerView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self addSubview:self.collectionView];

}

#pragma mark - collectionView dataSource Or delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [CCChannelManager sharedManager].confirmedChannels.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

#pragma mark - getters and setters

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, -SCREEN_HEIGHT/3*2, SCREEN_WIDTH, SCREEN_HEIGHT/3*2) collectionViewLayout:self.flowLayout];
        _collectionView.autoresizesSubviews = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        _collectionView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
    }
    return _collectionView;
}

-(UICollectionViewFlowLayout *)flowLayout{
    if (!_flowLayout) {
        _flowLayout = [[UICollectionViewFlowLayout alloc]init];
        [_flowLayout setItemSize:CGSizeMake(69, 30)];//设置cell的尺寸
        [_flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];//设置其布局方向
        _flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);//设置其边界
        _flowLayout.minimumInteritemSpacing = 7;
        _flowLayout.minimumLineSpacing = 15;
    }
    return _flowLayout;
}

@end
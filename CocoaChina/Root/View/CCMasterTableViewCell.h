//
//  CCMasterTableViewCell.h
//  CocoaChina
//
//  Created by Zhang on 15/10/30.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell/MGSwipeTableCell.h>
#import <MGSwipeTableCell/MGSwipeButton.h>
extern NSString *const kMasterCellIdentifier;
@class CCMasterModel;
@interface CCMasterTableViewCell : MGSwipeTableCell
@property (nonatomic, strong) CCMasterModel *model;
@end

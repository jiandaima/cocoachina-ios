//
//  AppDelegate.m
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCAppDelegate.h"
#import "MobClick.h"
#import "CCNavigationController.h"
#import "UMSocialData.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialSinaHandler.h"
#import "UMSocialSnsService.h"
#import "UMSocialConfig.h"
#import "UMFeedback.h"
#import "iRate.h"
#import "CCDBManager.h"
#import "UMessage.h"
#import "UMOpus.h"
#import "CCWebViewController.h"
@interface CCAppDelegate ()

@end

@implementation CCAppDelegate

+ (void)load{
    ///友盟设置
    [MobClick startWithAppkey:kUMeng_KEY reportPolicy:BATCH   channelId:@""];
    [MobClick setAppVersion:[YJTools appVersion]];
    //[MobClick setEncryptEnabled:YES];
    [MobClick setLogEnabled:YES];
    [MobClick setCrashReportEnabled:NO];
    ///友盟分享设置
    [UMSocialData setAppKey:kUMeng_KEY];
    [UMSocialWechatHandler setWXAppId:@"wxf6321078d711df5f" appSecret:@"d4624c36b6795d1d99dcf0547af5443d" url:@"http://www.umeng.com/social"];
    [UMSocialQQHandler setQQWithAppId:@"1104864435" appKey:@"98fc7FuuWwVV4O3M" url:@"http://www.umeng.com/social"];
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    [UMSocialConfig setFinishToastIsHidden:YES position:UMSocialiToastPositionTop];
    ///友盟用户反馈
    [UMFeedback setAppkey:kUMeng_KEY];
    
    ///评分系统
    [iRate sharedInstance].appStoreID = 1053468908;
    
    [Bmob registerWithAppKey:@"30c227263a0ffa6f76d829a5bd893cbd"];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    ///友盟意见反馈推送
    //register remoteNotification types
    UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
    action1.identifier = @"action1_identifier";
    action1.title=@"Accept";
    action1.activationMode = UIUserNotificationActivationModeForeground;//当点击的时候启动程序
    UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];  //第二按钮
    action2.identifier = @"action2_identifier";
    action2.title=@"Reject";
    action2.activationMode = UIUserNotificationActivationModeBackground;//当点击的时候不启动程序，在后台处理
    action2.authenticationRequired = YES;//需要解锁才能处理，如果action.activationMode = UIUserNotificationActivationModeForeground;则这个属性被忽略；
    action2.destructive = YES;
    
    UIMutableUserNotificationCategory *categorys = [[UIMutableUserNotificationCategory alloc] init];
    categorys.identifier = @"com.dami.CocoaChina";//这组动作的唯一标示
    [categorys setActions:@[action1,action2] forContext:(UIUserNotificationActionContextDefault)];
    
    UIUserNotificationSettings *userSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert categories:[NSSet setWithObject:categorys]];
    [UMessage registerRemoteNotificationAndUserNotificationSettings:userSettings];
    [UMessage setLogEnabled:NO];
    
    [[UMFeedback sharedInstance] setFeedbackViewController:nil shouldPush:YES];
    //关闭状态时点击反馈消息进入反馈页
    NSDictionary *notificationDict = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    [UMFeedback didReceiveRemoteNotification:notificationDict];
    //语音输入
    [UMOpus setAudioEnable:YES];
    
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = self.rootViewController;
    [self.window setTintColor:rgb(29, 173, 234)];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application { }

- (void)applicationDidEnterBackground:(UIApplication *)application {}

- (void)applicationWillEnterForeground:(UIApplication *)application {}

- (void)applicationDidBecomeActive:(UIApplication *)application {}

- (void)applicationWillTerminate:(UIApplication *)application {}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return  [UMSocialSnsService handleOpenURL:url];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    [UMFeedback didReceiveRemoteNotification:userInfo];
    if (application.applicationState == UIApplicationStateActive) {
        NSLog(@"active");
        //程序当前正处于前台
    }
    else if(application.applicationState == UIApplicationStateInactive)
    {
        NSLog(@"inactive");
        //程序处于后台
        
    }
//    CCWebViewController *webViewController = [[CCWebViewController alloc]init];
//    webViewController.masterModel =
}
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    BmobInstallation  *currentIntallation = [BmobInstallation currentInstallation];
    [currentIntallation setDeviceTokenFromData:deviceToken];
    [currentIntallation saveInBackground];
    
    [UMessage registerDeviceToken:deviceToken];
    NSLog(@"%@",deviceToken);
    NSLog(@"umeng message alias is: %@", [UMFeedback uuid]);
    [UMessage addAlias:[UMFeedback uuid] type:[UMFeedback messageType] response:^(id responseObject, NSError *error) {
        if (error != nil) {
            NSLog(@"%@", error);
            NSLog(@"%@", responseObject);
        }
    }];
}

#pragma mark - Drawer View Controllers

-(CCNavigationController *)rootViewController{
    if (!_rootViewController) {
        _rootViewController = [[CCNavigationController alloc] initWithRootViewController:self.centerViewController];
    }
    return _rootViewController;
}

-(CCRootViewController *)centerViewController{
    if (!_centerViewController) {
        _centerViewController = [CCRootViewController new];
    }
    return _centerViewController;
}

#pragma mark - Global Access Helper

+ (CCAppDelegate *)globalDelegate {
    return (CCAppDelegate *)[UIApplication sharedApplication].delegate;
}

@end

//
//  YJDataManager.m
//  yuedu
//
//  Created by Zhang on 15/9/20.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCDBManager.h"

#ifndef kDatabaseName

#define kDatabaseName @"myDatabase.db"

#endif

@interface CCDBManager ()

@end

@implementation CCDBManager

singleton_implementation(CCDBManager)

#pragma mark 重写初始化方法
-(instancetype)init{
    CCDBManager *manager;
    if((manager=[super init]))
    {
        [manager openDb:kDatabaseName];
        [self createHistroyTable];
        [self createShareTable];
        [self createFavoriteTable];
    }
    return manager;
}

-(void)openDb:(NSString *)dbname{
    //取得数据库保存路径，通常保存沙盒Documents目录
    NSString *directory=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"%@",directory);
    NSString *filePath=[directory stringByAppendingPathComponent:dbname];
    //创建FMDatabase对象
    //注意：dataWithPath中的路径参数一般会选择保存到沙箱中的Documents目录中；如果这个参数设置为nil则数据库会在内存中创建；如果设置为@””则会在沙箱中的临时目录创建,应用程序关闭则文件删除。
    self.database = [FMDatabaseQueue databaseQueueWithPath:filePath];
    //打开数据上
    if (self.database) {
        NSLog(@"数据库打开成功!");
    }else{
        NSLog(@"数据库打开失败!");
    }
}

///无返回结果
-(void)executeNonQuery:(NSString *)sql parameterDictionary:(NSDictionary *)parameters{
    //执行更新sql语句，用于插入、修改、删除
    [self.database inDatabase:^(FMDatabase *db) {
        [db executeUpdate:sql withParameterDictionary:parameters];
    }];
}

///有返回结果
-(NSArray *)executeQuery:(NSString *)sql parameterDictionary:(NSDictionary *)parameters{
    NSMutableArray *array=[NSMutableArray array];
    [self.database inDatabase:^(FMDatabase *db) {
        //执行查询sql语句
        FMResultSet *result= [db executeQuery:sql withParameterDictionary:parameters];
        while (result.next) {
            NSMutableDictionary *dic=[NSMutableDictionary dictionary];
            for (int i=0; i<result.columnCount; ++i) {
                dic[[result columnNameForIndex:i]]=[result stringForColumnIndex:i];
            }
            [array addObject:dic];
        }
    }];
    return array;
}

#pragma mark -  CREATE

-(void)createHistroyTable{
    NSString *sql = @"CREATE TABLE IF NOT EXISTS history_table (\
                        newsId varchar(128) PRIMARY KEY NOT NULL,\
                        readdate double NOT NULL,\
                        data varchar(1024) NOT NULL\
                    );";
    [[CCDBManager sharedCCDBManager]executeNonQuery:sql parameterDictionary:nil];
}

-(void)createFavoriteTable{
    NSString *sql = @"CREATE TABLE IF NOT EXISTS favorite_table (\
    newsId varchar(128) PRIMARY KEY NOT NULL,\
    readdate double NOT NULL,\
    data varchar(1024) NOT NULL\
    );";
    [[CCDBManager sharedCCDBManager]executeNonQuery:sql parameterDictionary:nil];
}

-(void)createShareTable{
    NSString *sql = @"CREATE TABLE IF NOT EXISTS share_table (\
    newsId varchar(128) PRIMARY KEY NOT NULL,\
    readdate double NOT NULL,\
    data varchar(1024) NOT NULL\
    );";
    [[CCDBManager sharedCCDBManager]executeNonQuery:sql parameterDictionary:nil];
}

////插入阅读历史
//-(void)insertReadHistoryWithNewsId:(NSString *)newsId{
//    NSMutableString *sAttributeName = [[NSMutableString alloc] initWithFormat:@""];
//    NSMutableString *sAttributeQ = [[NSMutableString alloc] initWithFormat:@""];
//    NSMutableArray *arrArgs = [[NSMutableArray alloc] initWithCapacity:0];
//    //id
//    [sAttributeName appendFormat:@"newsId"];
//    [sAttributeQ appendFormat:@"?"];
//    [arrArgs addObject:newsId];
//
//    NSString *sSQL = [[NSString alloc] initWithFormat:SQL_INSERT_ARRAY,t_read_history,sAttributeName,sAttributeQ];
//    [self insertToTableWithInsertSQL:sSQL withArray:arrArgs];
//}
//
//-(void)insertReadHistroy:(YJReadHistoryModel *)historyModel{
//    
//    NSString *sql = @"INSERT INTO history_table (newsId,newsLink,cDate,)";
//    NSMutableArray *arguments;
//    [self.dbQueue inDatabase:^(FMDatabase *db) {
//        //NSLog(@"%@",sql);
//        BOOL result = [db executeUpdate:sql withArgumentsInArray:arguments];
//        if(!result)
//        {
//            //NSLog(@"insertToTable error");
//        }
//    }];
//}
//
//-(BOOL)checkReadHistroyWithNewsId:(NSString *)newsId{
//    NSString *sql = [NSString stringWithFormat:QUERY_ALL_FROM_TABLE_WITH_WHERE,t_read_history,@"newsId"];
//    NSArray *resultArray = [self executeQuery:sql withArray:@[newsId]];
//    return resultArray.count > 0;
//}
//
//- (void)createTableWithCreateSQL:(NSString *)createSQL withTableName:(NSString *)tableName{
//    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
//    if ([db open]){
//        NSString *sSQL = [NSString stringWithFormat:createSQL,tableName];
//        BOOL res = [db executeUpdate:sSQL];
//        if (!res){
//            DDLogInfo(@"error when createOtherTable");
//        }else{
//            DDLogInfo(@"succ to createOtherTable");
//        }
//        [db close];
//    }else{
//        DDLogInfo(@"error when open  createOtherTable db");
//    }
//}


////插入元素
//-(void)insertToTableWithInsertSQL:(NSString *)sql withArray:(NSArray *)array{
//    
//    [self.dbQueue inDatabase:^(FMDatabase *db) {
//        //NSLog(@"%@",sql);
//        BOOL result = [db executeUpdate:sql withArgumentsInArray:array];
//        if(!result)
//        {
//            //NSLog(@"insertToTable error");
//        }
//    }];
//}
//
//#pragma mark - getters and setters
//-(NSString *)dbPath{
//    if (!_dbPath) {
//        NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//        _dbPath = [docsPath stringByAppendingPathComponent:@"damDB.db"];
//    }
//    return _dbPath;
//}

@end

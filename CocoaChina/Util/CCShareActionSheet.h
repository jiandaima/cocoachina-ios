//
//  YJShareManager.h
//  yuedu
//
//  Created by Zhang on 15/9/19.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "UMSocial.h"

///分享类型
typedef NS_ENUM(NSUInteger, CCShareType) {
    ///分享到新浪微博
    YJShareTypeSina = 0,
    ///分享到微信
    YJShareTypeWeiXin,
    ///分享到朋友圈
    YJShareTypePengYouQuan,
    ///分享到QQ
    YJShareTypeQQ,
    ///分享到QQ空间
    YJShareTypeQQZone,
    YJShareTypeSafari,
    YJShareTypeCopy,
};

@protocol CCShareActionSheetDelegate <NSObject>

@optional

@end

@interface CCShareActionSheet : NSObject

+ (instancetype)shareServiceWithController:(UIViewController *)controller title:(NSString *)title context:(NSString *)content imageLink:(NSString *)imageLink topicLink:(NSString *)topicLink delegate:(id<CCShareActionSheetDelegate>)delegate;

-(instancetype)initWithController:(UIViewController *)controller title:(NSString *)title content:(NSString *)content imageLink:(NSString *)imageLink topicLink:(NSString *)topicLink delegate:(id<CCShareActionSheetDelegate>)delegate;
///展示
-(void)show;

///隐藏
-(void)dismiss;

@end



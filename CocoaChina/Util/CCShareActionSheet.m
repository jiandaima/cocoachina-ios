//
//  YJShareManager.m
//  yuedu
//
//  Created by Zhang on 15/9/19.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCShareActionSheet.h"
#import "CCShareItemCell.h"
#import "UMSocial.h"
#import "SDWebImageManager.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "CCAppDelegate.h"
#import "UIImageView+WebCache.h"

#define kTagFontSize 10
#define kTagTraffic 11

static CCShareActionSheet *instance;

@interface CCShareActionSheet () <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UMSocialUIDelegate>
/// 半透明底色背景图
@property(nonatomic, strong) UIView *backgroundView;
/// shareContent 分享内容视图
@property(nonatomic, strong) UIView *shareContentView;
/// 放置个分享平台的cell
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) UIViewController *controller;
///标题
@property(nonatomic, strong) NSString *title;
///内容
@property(nonatomic, strong) NSString *content;
///图片链接
@property(nonatomic, strong) NSString *imageLink;
///文章链接
@property(nonatomic, strong) NSString *topicLink;
///代理
@property(nonatomic, weak) id<CCShareActionSheetDelegate> delegate;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) MASConstraint *topConstraint;

@end

@implementation CCShareActionSheet

#pragma mark - Init

+ (instancetype)shareServiceWithController:(UIViewController *)controller title:(NSString *)title context:(NSString *)content imageLink:(NSString *)imageLink topicLink:(NSString *)topicLink delegate:(id<CCShareActionSheetDelegate>)delegate {
    
    return [[CCShareActionSheet alloc] initWithController:controller title:title content:content imageLink:imageLink topicLink:topicLink delegate:delegate];
}

- (instancetype)initWithController:(UIViewController *)controller title:(NSString *)title content:(NSString *)content imageLink:(NSString *)imageLink topicLink:(NSString *)topicLink delegate:(id<CCShareActionSheetDelegate>)delegate {
    self = [super init];
    if (self) {
        _controller = controller;
        _title = title;
        _content = content;
        _imageLink = imageLink;
        _topicLink = topicLink;
        _delegate = delegate;
        [self commonInit];
    }
    return self;
}

#pragma mark - CommonInit

- (void)commonInit {
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:self.backgroundView];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGestureClicked:)];
    [self.backgroundView addGestureRecognizer:tapGestureRecognizer];
    // 分享内容主视图
    [window addSubview:self.shareContentView];
    [self.shareContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo([UIScreen mainScreen].bounds.size);
        self.topConstraint = make.top.equalTo(self.backgroundView.mas_bottom);
    }];
    
    [self.shareContentView addSubview:self.collectionView];
    [self.shareContentView addSubview:self.cancelButton];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 200));
        make.top.equalTo(self.shareContentView.mas_top);
    }];
    //取消按钮
    
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom);
        make.right.left.equalTo(self.shareContentView);
        make.height.equalTo(@(45));
    }];
}

#pragma mark - Private

- (void)show {
    instance = self;
    [self.shareContentView layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.topConstraint.offset(-245);
        self.backgroundView.alpha = 0.5;
        [self.shareContentView layoutIfNeeded];
    }];
}

- (void)dismiss {
    [self.shareContentView layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundView.alpha = 0;
        self.topConstraint.offset(0);
        [self.shareContentView layoutIfNeeded];
    } completion:^(BOOL finished){
        [self.shareContentView removeFromSuperview];
        [self.backgroundView removeFromSuperview];
        [UMSocialControllerService defaultControllerService].socialUIDelegate = nil;
    }];
}

#pragma mark - Actions
-(void)onCancelButtonClicked:(UIButton *)sender{
    [self dismiss];
    instance = nil;
}

-(void)onTapGestureClicked:(UITapGestureRecognizer *)tapGesture{
    [self dismiss];
    instance = nil;
}

#pragma mark - UICollectionView delegate & datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CCShareItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDAMShareItemCellIdentifier forIndexPath:indexPath];
    CCShareItemModel *itemModel = self.dataSource[indexPath.row];
    cell.itemModel = itemModel;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(SCREEN_WIDTH / 4, 90);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 0, 10, 0);
}

-(void)openInSafari{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.topicLink]]) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.topicLink]];
    }
}

-(void)copyLink{
    [[UIPasteboard generalPasteboard] setPersistent:YES];
    [[UIPasteboard generalPasteboard] setValue:self.topicLink forPasteboardType:[UIPasteboardTypeListString objectAtIndex:0]];
    [JDStatusBarNotification showWithStatus:@"链接已拷贝" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
}

-(void)shareToSina{
    [[UIImageView new] sd_setImageWithURL:[NSURL URLWithString:self.imageLink] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        if (image == nil) {
            imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
        }
        if (self.title == nil) {
            self.title = @" ";
        }
        ///新浪
        NSString *sinaShareText = [NSString stringWithFormat:@"%@ %@", self.title, self.topicLink];
        [[UMSocialControllerService defaultControllerService] setShareText:sinaShareText shareImage:imageData socialUIDelegate:self];        //设置分享内容和回调对象
        CCAppDelegate *appDel = [UIApplication sharedApplication].delegate;
        [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina].snsClickHandler(appDel.rootViewController.topViewController,[UMSocialControllerService defaultControllerService],YES);
    }];
}

-(void)shareToWeChat{
    [[UIImageView new] sd_setImageWithURL:[NSURL URLWithString:self.imageLink] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        if (image == nil) {
            imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
        }
        if (self.title == nil) {
            self.title = @" ";
        }
        // 微信好友
        if ([WXApi isWXAppInstalled] == NO) {
            NSURL *url = [NSURL URLWithString:[WXApi getWXAppInstallUrl]];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
            return;
        }
        [UMSocialData defaultData].extConfig.wechatSessionData.wxMessageType = UMSocialWXMessageTypeNone;
        [UMSocialData defaultData].extConfig.wechatSessionData.url = self.topicLink;
        [UMSocialData defaultData].extConfig.wechatSessionData.title = self.title;
        [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToWechatSession ] content:self.content image:imageData location:nil urlResource:nil presentedController:self.controller completion:^(UMSocialResponseEntity *response) {
            if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
                [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
            }else{
                [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleError];
            }
        }];
        instance = nil;
    }];
}

-(void)shareToTimeLine{
    [[UIImageView new] sd_setImageWithURL:[NSURL URLWithString:self.imageLink] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        if (image == nil) {
            imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
        }
        if (self.title == nil) {
            self.title = @" ";
        }
        // 微信朋友圈
        if ([WXApi isWXAppInstalled] == NO) {
            NSURL *url = [NSURL URLWithString:[WXApi getWXAppInstallUrl]];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
            return;
        }
        [UMSocialData defaultData].extConfig.wechatTimelineData.wxMessageType = UMSocialWXMessageTypeWeb;
        [UMSocialData defaultData].extConfig.wechatTimelineData.url = self.topicLink;
        [UMSocialData defaultData].extConfig.wechatTimelineData.title = self.title;
        [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToWechatTimeline ] content:self.content image:imageData location:nil urlResource:nil presentedController:self.controller completion:^(UMSocialResponseEntity *response) {
            //2241、2521 #（bug号） by  whq（修改人） at 201500603（修改日期） begin
            if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
                [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
            }else{
                [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
            }
        }];
        instance = nil;
    }];
}

-(void)shareToQQ{
    [[UIImageView new] sd_setImageWithURL:[NSURL URLWithString:self.imageLink] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        if (image == nil) {
            imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
        }
        if (self.title == nil) {
            self.title = @" ";
        }
        // QQ好友
        [UMSocialData defaultData].extConfig.qqData.url = self.topicLink;
        [UMSocialData defaultData].extConfig.qqData.title = self.title;
        [UMSocialData defaultData].extConfig.qqData.qqMessageType = UMSocialQQMessageTypeDefault;
        self.content = [NSString stringWithFormat:@"%@", self.content];
        [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToQQ ] content:self.content image:imageData location:nil urlResource:nil presentedController:self.controller completion:^(UMSocialResponseEntity *response) {
            if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
                [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
            }else{
                [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
            }
        }];
        instance = nil;
    }];
}

-(void)shareToQQZone{
    [[UIImageView new] sd_setImageWithURL:[NSURL URLWithString:self.imageLink] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        if (image == nil) {
            imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"icon_logo"], 1.0);
        }
        if (self.title == nil) {
            self.title = @" ";
        }
        // QQ空间
        [UMSocialData defaultData].extConfig.qzoneData.url = self.topicLink;
        [UMSocialData defaultData].extConfig.qzoneData.title = self.title;
        self.content = [NSString stringWithFormat:@"%@", self.content];
        [[UMSocialDataService defaultDataService] postSNSWithTypes:@[ UMShareToQzone ] content:self.content image:imageData location:nil urlResource:nil presentedController:self.controller completion:^(UMSocialResponseEntity *response) {
            if (response.responseType == UMSResponseShareToMutilSNS && response.responseCode == UMSResponseCodeSuccess) {
                [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
            }else{
                [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
            }
        }];
        instance = nil;
    }];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
    [self dismiss];
    CCShareItemModel *shareItem = self.dataSource[indexPath.row];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:shareItem.action];
#pragma clang diagnostic pop
    
}


-(NSArray *)dataSource{
    if (!_dataSource) {
        CCShareItemModel *itemModel0 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"新浪微博",@"image":@"share_platform_sina",@"type":@0}];
        itemModel0.action = @selector(shareToSina);
        CCShareItemModel *itemModel1 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"微信好友",@"image":@"share_platform_wechat",@"type":@1}];
        itemModel1.action = @selector(shareToWeChat);
        CCShareItemModel *itemModel2 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"微信朋友圈",@"image":@"share_platform_wechattimeline",@"type":@2}];
        itemModel2.action = @selector(shareToTimeLine);
        CCShareItemModel *itemModel3 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"QQ好友",@"image":@"share_platform_qqfriends",@"type":@3}];
        itemModel3.action = @selector(shareToQQ);
        CCShareItemModel *itemModel4 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"QQ空间",@"image":@"share_platform_qzone",@"type":@4}];
        itemModel4.action = @selector(shareToQQZone);
        CCShareItemModel *itemModel5 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"用Safari打开",@"image":@"share_safari",@"type":@5}];
        itemModel5.action = @selector(openInSafari);
        CCShareItemModel *itemModel6 = [[CCShareItemModel alloc]initWithDictionary:@{@"title":@"拷贝链接",@"image":@"share_copy",@"type":@6}];
        itemModel6.action = @selector(copyLink);
        if([QQApiInterface isQQInstalled]){
            _dataSource = @[itemModel0,itemModel1,itemModel2,itemModel3,itemModel4,itemModel5,itemModel6];
        }else{
            _dataSource = @[itemModel0,itemModel1,itemModel2,itemModel5,itemModel6];
        }
    }
    return _dataSource;
}

- (void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response {
    if (response.viewControllerType == UMSViewControllerOauth) {
        if (response.responseType == UMSViewControllerShareEdit && response.responseCode == UMSResponseCodeSuccess) {
            [JDStatusBarNotification showWithStatus:@"分享成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
        }else{
            [JDStatusBarNotification showWithStatus:@"分享失败" dismissAfter:2 styleName:JDStatusBarStyleError];
        }
        // NSLog(@"didFinishOauthAndGetAccount response is %@",response);
    }
    //    instance = nil;
}

-(UIView *)backgroundView{
    if (!_backgroundView) {
        _backgroundView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backgroundView.backgroundColor = [UIColor blackColor];
        _backgroundView.alpha = 0;
    }
    return _backgroundView;
}

-(UIView *)shareContentView{
    if (!_shareContentView) {
        _shareContentView = [[UIView alloc] init];
        _shareContentView.backgroundColor = [UIColor yj_cellBackgroundColor];
    }
    return _shareContentView;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor yj_cellBackgroundColor];
        [_collectionView registerClass:[CCShareItemCell class] forCellWithReuseIdentifier:kDAMShareItemCellIdentifier];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}
-(UIButton *)cancelButton{
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelButton addTarget:self action:@selector(onCancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor yj_darkTextColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_cellBackgroundColor]] forState:UIControlStateNormal];
        [_cancelButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_cellSelectedColor]] forState:UIControlStateHighlighted];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:18];
        
    }
    return _cancelButton;
}

@end
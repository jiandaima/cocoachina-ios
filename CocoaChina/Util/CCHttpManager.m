//
//  YJHttpManager.m
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCHttpManager.h"
#import <AFNetworking/AFNetworking.h>
#import "AFNetworkActivityIndicatorManager.h"

@implementation CCHttpManager

+ (instancetype)sharedManager{
    static CCHttpManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[CCHttpManager alloc]initWithBaseURL:nil];
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
        responseSerializer.removesKeysWithNullValues = YES;
        responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
        _sharedManager.responseSerializer = responseSerializer;
        _sharedManager.requestSerializer.timeoutInterval = 25;
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    });
    
    return _sharedManager;
}

- (AFHTTPRequestOperation *)yj_get:(NSString *)urlPath parameters:(NSDictionary *)parameters compliteBlock:(void (^)(AFHTTPRequestOperation *, id, NSError *))compliteBlock{
    
    return [self yj_get:urlPath parameters:parameters loadCache:NO saveCache:NO compliteBlock:compliteBlock];
}

- (AFHTTPRequestOperation *)yj_get:(NSString *)urlPath parameters:(NSDictionary *)parameters loadCache:(BOOL)loadCache saveCache:(BOOL)saveCache  compliteBlock:(void (^)(AFHTTPRequestOperation *,id , NSError *))compliteBlock{
    
    if (loadCache) {
        NSString *cacheKey = [[CCCacheManager sharedManager]cacheKeyForURL:urlPath parameter:parameters];
        id obj = [[CCCacheManager sharedManager] cacheforKey:cacheKey];
        if (obj) {
            NSLog(@"\nRespones:%@\nParameters:%@",urlPath,obj);
            compliteBlock(nil,obj,nil);
            return nil;
        }
    }
    //不加载缓存或缓存加载失败
    AFHTTPRequestOperation *requestOperation = [self GET:urlPath parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        NSString *httpString = [NSString stringWithFormat:@"\n[Get]\nRequest:%@\nParameters:%@\nHeaders:%@\nResponse:%@",operation.request.URL.absoluteString,parameters,operation.request.allHTTPHeaderFields,responseObject];
        
        NSLog(@"%@",httpString);
        
        if (compliteBlock) {
            compliteBlock(operation,responseObject,nil);
        }
        
        if (saveCache && responseObject) {
            [[CCCacheManager sharedManager]saveCacheData:responseObject withUrlPath:urlPath andParameters:parameters];
        }
        ///获取数据失败时调用
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        if (error.code == NSURLErrorCancelled) {
            return ;
        }
        if(saveCache){
            [[CCCacheManager sharedManager]cacheForPath:urlPath parameters:parameters completion:^(id cacheObject) {
                compliteBlock(operation,cacheObject,error);
            }];
        }else{
            compliteBlock(operation,nil,error);
        }
    }];
    return requestOperation;
}

- (AFHTTPRequestOperation *)yj_post:(NSString *)urlPath parameters:(NSDictionary *)parameters compliteBlock:(void (^)(AFHTTPRequestOperation *, id, NSError *))compliteBlock{
    return [self yj_post:urlPath parameters:parameters loadCache:NO saveCache:NO compliteBlock:compliteBlock];
}

- (AFHTTPRequestOperation *)yj_post:(NSString *)urlPath parameters:(NSDictionary *)parameters loadCache:(BOOL)loadCache saveCache:(BOOL)saveCache  compliteBlock:(void (^)(AFHTTPRequestOperation *,id , NSError *))compliteBlock{
    
    if (loadCache) {
        NSString *cacheKey = [[CCCacheManager sharedManager]cacheKeyForURL:urlPath parameter:parameters];
        id obj = [[CCCacheManager sharedManager] cacheforKey:cacheKey];
        if (obj) {
            NSLog(@"\nRespones:%@\nParameters:%@",urlPath,obj);
            compliteBlock(nil,obj,nil);
            return nil;
        }
    }
    //不加载缓存或缓存加载失败
    AFHTTPRequestOperation *requestOperation = [self POST:urlPath parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        NSString *httpString = [NSString stringWithFormat:@"\n[Get]\nRequest:%@\nParameters:%@\nHeaders:%@\nResponse:%@",operation.request.URL.absoluteString,parameters,operation.request.allHTTPHeaderFields,responseObject];
        
        NSLog(@"%@",httpString);
        
        if (compliteBlock) {
            compliteBlock(operation,responseObject,nil);
        }
        
        if (saveCache && responseObject) {
            [[CCCacheManager sharedManager]saveCacheData:responseObject withUrlPath:urlPath andParameters:parameters];
        }
        ///获取数据失败时调用
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        if (error.code == NSURLErrorCancelled) {
            return ;
        }
        if(saveCache){
            [[CCCacheManager sharedManager]cacheForPath:urlPath parameters:parameters completion:^(id cacheObject) {
                compliteBlock(operation,cacheObject,error);
            }];
        }else{
            compliteBlock(operation,nil,error);
        }
    }];
    return requestOperation;
}

@end

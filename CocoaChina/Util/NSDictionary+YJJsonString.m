//
//  NSDictionary+YJJsonString.m
//  yuedu
//
//  Created by Zhang on 15/10/25.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "NSDictionary+YJJsonString.h"

@implementation NSDictionary (YJJsonString)

-(NSString*)toJSONString{
    NSString *jsonString = nil;
    NSError *error;
    // Pass 0 if you don't care about the readability of the generated string
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

+(instancetype )dictionaryFromJSONString:(NSString *)jsonString{
    NSData* data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end

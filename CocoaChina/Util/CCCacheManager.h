//
//  YJCacheManager.h
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^DAMCacheBlock)(id cacheObject);

@interface CCCacheManager : NSObject

+ (instancetype)sharedManager;

- (id)cacheforKey:(NSString *)key;

- (NSString *)cacheKeyForURL:(NSString *)urlString parameter:(NSDictionary *)parameter;

- (BOOL)saveCacheData:(id)data withUrlPath:(NSString *)urlString andParameters:(NSDictionary *)parameters;

- (void)cacheForPath:(NSString *)path parameters:(NSDictionary *)parameters completion:(DAMCacheBlock)comleteBlock;
@end

//
//  YJDBMacros.h
//  YJ6
//
//  Created by ZhangAimin on 14/9/13.
//  Copyright (c) 2014年 爱卡汽车. All rights reserved.
//

/**
 数据库相关宏定义
 */
#define t_read_history @"t_read_history"
//创建阅读历史表
#define t_read_history_sql @"CREATE TABLE IF NOT EXISTS t_read_history (\
                            channelId Varchar DEFAULT NULL,\
                            newsId Varchar  PRIMARY KEY DEFAULT NULL)"

#pragma mark - INSERT
//插入数据 用来生成插入语句
#define SQL_INSERT_ARRAY @"REPLACE INTO %@ (%@) VALUES (%@)"

#pragma mark - QUERY
//查询语句 指定键值 指定排序
#define QUERY_FROM_TABLE_WITH_WHERE_AND_OREDER_BY @"Select * from %@ where %@ = ? ORDER by %@"
//查询语句 指定键值 无排序
#define QUERY_ALL_FROM_TABLE_WITH_WHERE @"Select * from %@ where %@ = ?"
//通过指定的newsid和newstype查找文章列表项
#define QUERY_ARTICLELIST_BY_NEWSID_NEWSTYPE @"SELECT * FROM T_ARTICLELIST WHERE newsId = ? AND newsType = ?"




static NSString* const adwoResponseErrorInfoList[] = {
    @"操作成功",
    @"广告初始化失败",
    @"当前广告已调用了加载接口",
    @"不该为空的参数为空",
    @"参数值非法",
    @"非法广告对象句柄",
    @"代理为空或adwoGetBaseViewController方法没实现",
    @"非法的广告对象句柄引用计数",
    @"意料之外的错误",
    @"广告请求太过频繁",
    @"广告加载失败",
    @"全屏广告已经展示过",
    @"全屏广告还没准备好来展示",
    @"全屏广告资源破损",
    @"开屏全屏广告正在请求",
    @"当前全屏已设置为自动展示",
    @"当前事件触发型广告已被禁用",
    @"没找到相应合法尺寸的事件触发型广告",
    
    @"服务器繁忙",
    @"当前没有广告",
    @"未知请求错误",
    @"PID不存在",
    @"PID未被激活",
    @"请求数据有问题",
    @"接收到的数据有问题",
    @"当前IP下广告已经投放完",
    @"当前广告都已经投放完",
    @"没有低优先级广告",
    @"开发者在Adwo官网注册的Bundle ID与当前应用的Bundle ID不一致",
    @"服务器响应出错",
    @"设备当前没连网络，或网络信号不好",
    @"请求URL出错"
};
//
//  shareItemCell.m
//  YJ6
//
//  Created by 高伟 on 14/12/11.
//  Copyright (c) 2014年 爱卡汽车. All rights reserved.
//

#import "CCShareItemCell.h"

NSString *const kDAMShareItemCellIdentifier = @"kDAMShareItemCellIdentifier";

@interface CCShareItemCell ()
/// 分享平台的名称
@property (nonatomic, strong)UILabel *textLabel;
/// 分享平台的图标
@property (nonatomic, strong)UIImageView *imageView;
@end

@implementation CCShareItemCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.textLabel];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.centerY.equalTo(self.contentView.mas_centerY).offset(-15);
        }];
        
        [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.top.equalTo(self.imageView.mas_bottom).offset(12);
        }];
    }
    return self;
}

-(void)setItemModel:(CCShareItemModel *)itemModel{
    _itemModel = itemModel;
    self.textLabel.text = itemModel.title;
    self.imageView.image = [UIImage imageNamed:itemModel.image];
}

#pragma mark - getters and setters
-(UILabel *)textLabel{
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.font = [UIFont systemFontOfSize:13];
    }
    return _textLabel;
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        
    }
    return _imageView;
}

@end

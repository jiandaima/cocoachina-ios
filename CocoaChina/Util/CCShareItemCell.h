//
//  shareItemCell.h
//  YJ6
//
//  Created by 高伟 on 14/12/11.
//  Copyright (c) 2014年 爱卡汽车. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCShareItemModel.h"

extern NSString *const kDAMShareItemCellIdentifier;

@interface CCShareItemCell : UICollectionViewCell
@property (nonatomic, strong) CCShareItemModel *itemModel;

@end
